/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class ChiTietPhieuNhap {
    private String SoPN;
    private String MaNL;
    private double DonGiaNhap;
    private int SoLuong;

    public ChiTietPhieuNhap(String SoPN, String MaNL, double DonGiaNhap, int SoLuong) {
        this.SoPN = SoPN;
        this.MaNL = MaNL;
        this.DonGiaNhap = DonGiaNhap;
        this.SoLuong = SoLuong;
    }

    public String getSoPN() {
        return SoPN;
    }

    public void setSoPN(String SoPN) {
        this.SoPN = SoPN;
    }

    public String getMaNL() {
        return MaNL;
    }

    public void setMaNL(String MaNL) {
        this.MaNL = MaNL;
    }

    public double getDonGiaNhap() {
        return DonGiaNhap;
    }

    public void setDonGiaNhap(double DonGiaNhap) {
        this.DonGiaNhap = DonGiaNhap;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }
}
