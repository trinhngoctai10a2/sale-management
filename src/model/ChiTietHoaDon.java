/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.SQLException;

/**
 *
 * @author Admin
 */
public class ChiTietHoaDon {
    private String SoHD;
    private String MaMA;
    private long DonGiaBan;
    private int SoLuong;

    public ChiTietHoaDon(String SoHD, String MaMA, long DonGiaBan, int SoLuong) {
        this.SoHD = SoHD;
        this.MaMA = MaMA;
        this.DonGiaBan = DonGiaBan;
        this.SoLuong = SoLuong;
    }

    public String getSoHD() {
        return SoHD;
    }

    public void setSoHD(String SoHD) {
        this.SoHD = SoHD;
    }

    public String getMaMA() {
        return MaMA;
    }

    public void setMaMA(String MaMA) {
        this.MaMA = MaMA;
    }

    public long getDonGiaBan() {
        return DonGiaBan;
    }

    public void setDonGiaBan(long DonGiaBan) {
        this.DonGiaBan = DonGiaBan;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }
    
    public MonAn getMonAn() throws SQLException {
        return new QuanLyMonAn().find(this.MaMA);
    }
}
