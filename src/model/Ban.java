/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class Ban {
    private String SoBan;
    private String TenBan;
    private String TrangThai;
    private String GhiChu;

    public Ban(String SoBan, String TenBan, String TrangThai, String GhiChu) {
        this.SoBan = SoBan;
        this.TenBan = TenBan;
        this.TrangThai = TrangThai;
        this.GhiChu = GhiChu;
    }

    public String getSoBan() {
        return SoBan;
    }

    public void setSoBan(String SoBan) {
        this.SoBan = SoBan;
    }

    public String getTenBan() {
        return TenBan;
    }

    public void setTenBan(String TenBan) {
        this.TenBan = TenBan;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String GhiChu) {
        this.GhiChu = GhiChu;
    }
}
