/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class QuanLyNhanVien {
    private final Statement st;

    public QuanLyNhanVien() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<NhanVien> getAll(String search) throws SQLException {
        ArrayList<NhanVien> nv;
        String sql = "SELECT * FROM NhanVien";
        
        if(!search.equals("")){
            sql += " WHERE MaNV LIKE '%" + search + "%' OR HoNV LIKE N'%" + search + "%' OR TenNV LIKE N'%" + search + "%'";
        }
        
        ResultSet rs = this.st.executeQuery(sql);
        
        nv = new ArrayList<>();
        
        while(rs.next()){
            String manv, honv, tennv, gioitinh, diachi, dienthoai, noisinh, cmnd;
            Date ngaysinh, ngayvaolam;
            
            manv = rs.getString("MaNV");
            honv = rs.getString("HoNV");
            tennv = rs.getString("TenNV");
            gioitinh = rs.getString("GioiTinh");
            ngaysinh = rs.getDate("NgaySinh");
            diachi = rs.getString("DiaChi");
            dienthoai = rs.getString("DienThoai");
            noisinh = rs.getString("NoiSinh");
            ngayvaolam = rs.getDate("NgayVaoLam");
            cmnd = rs.getString("CMND");
            
            nv.add(new NhanVien(manv, honv, tennv, gioitinh, ngaysinh, diachi, dienthoai, noisinh, ngayvaolam, cmnd));
        }
        return nv;
    }
    
    public boolean add(NhanVien nv) throws SQLException {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
        
        String sql = "INSERT INTO NhanVien VALUES('" + nv.getMaNV() + "', N'" + nv.getHoNV() + "', N'" + nv.getTenNV() + "', N'" + nv.getGioiTinh() + "', '" + formater.format(nv.getNgaySinh()) + "', N'" + nv.getDiaChi() + "', '" + nv.getDienThoai() +"', N'" + nv.getNoiSinh() + "', '" + formater.format(nv.getNgayVaoLam()) + "', '" + nv.getCMND() + "')";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean update(NhanVien nv) throws SQLException{
        SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
        
        String sql = "UPDATE NhanVien SET HoNV = N'" + nv.getHoNV() + "', TenNV = N'" + nv.getTenNV() + "', GioiTinh = N'" + nv.getGioiTinh() + "', NgaySinh = '" + formater.format(nv.getNgaySinh()) + "', DiaChi = N'" + nv.getDiaChi() + "', DienThoai = '" + nv.getDienThoai() +"', NoiSinh = N'" + nv.getNoiSinh() + "', NgayVaoLam = '" + formater.format(nv.getNgayVaoLam()) + "', CMND = '" + nv.getCMND() + "' WHERE MaNV = '" + nv.getMaNV() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String manv) throws SQLException {
        String sql = "EXEC sp_XoaNhanVien '" + manv + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public NhanVien find(String manv) throws SQLException {
        String sql = "SELECT * FROM NhanVien WHERE MaNV = '" + manv + "'";
        ResultSet rs = this.st.executeQuery(sql);
        
        String honv, tennv, gioitinh, diachi, dienthoai, noisinh, cmnd;
        Date ngaysinh, ngayvaolam;

        rs.first();
        honv = rs.getString("HoNV");
        tennv = rs.getString("TenNV");
        gioitinh = rs.getString("GioiTinh");
        ngaysinh = rs.getDate("NgaySinh");
        diachi = rs.getString("DiaChi");
        dienthoai = rs.getString("DienThoai");
        noisinh = rs.getString("NoiSinh");
        ngayvaolam = rs.getDate("NgayVaoLam");
        cmnd = rs.getString("CMND");
            
        return new NhanVien(manv, honv, tennv, gioitinh, ngaysinh, diachi, dienthoai, noisinh, ngayvaolam, cmnd);
    }
}
