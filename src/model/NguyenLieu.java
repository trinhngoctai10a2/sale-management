/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class NguyenLieu {
    private String MaNL;
    private String TenNL;
    private String DonViTinh;
    private int TonKho;
    private String GhiChu;

    public NguyenLieu(String MaNL, String TenNL, String DonViTinh, int TonKho, String GhiChu) {
        this.MaNL = MaNL;
        this.TenNL = TenNL;
        this.DonViTinh = DonViTinh;
        this.TonKho = TonKho;
        this.GhiChu = GhiChu;
    }

    public String getMaNL() {
        return MaNL;
    }

    public void setMaNL(String MaNL) {
        this.MaNL = MaNL;
    }

    public String getTenNL() {
        return TenNL;
    }

    public void setTenNL(String TenNL) {
        this.TenNL = TenNL;
    }

    public String getDonViTinh() {
        return DonViTinh;
    }

    public void setDonViTinh(String DonViTinh) {
        this.DonViTinh = DonViTinh;
    }

    public int getTonKho() {
        return TonKho;
    }

    public void setTonKho(int TonKho) {
        this.TonKho = TonKho;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String GhiChu) {
        this.GhiChu = GhiChu;
    }
}
