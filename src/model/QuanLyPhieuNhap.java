/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class QuanLyPhieuNhap {
    private final Statement st;

    public QuanLyPhieuNhap() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<PhieuNhap> getAll(String search) throws SQLException {
        ArrayList<PhieuNhap> pn;
        String sql = "SELECT * FROM PhieuNhap";
        
        if(!search.equals("")){
            sql += " WHERE SoPN LIKE '%" + search + "%' OR MaNV LIKE N'%" + search + "%' OR MaNCC LIKE N'%" + search + "%'";
        }
        ResultSet rs = this.st.executeQuery(sql);
        
        pn = new ArrayList<>();
        
        while(rs.next()){
            String sopn, manv, mancc, ghichu;
            double tongtrigia;
            Date ngaynhap;
            
            sopn = rs.getString("SoPN");
            manv = rs.getString("MaNV");
            mancc = rs.getString("MaNCC");
            ngaynhap = rs.getDate("NgayNhap");
            tongtrigia = rs.getDouble("TongTriGia");
            ghichu = rs.getString("GhiChu");
            
            pn.add(new PhieuNhap(sopn, manv, mancc, ngaynhap, tongtrigia, ghichu));
        }
        
        return pn;
    }
    
    public String add(PhieuNhap pn) throws SQLException {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
        
        String sql = "INSERT INTO PhieuNhap (MaNV, MaNCC, NgayNhap, TongTriGia, GhiChu) OUTPUT INSERTED.SoPN VALUES('" + pn.getMaNV()+ "', '" + pn.getMaNCC()+ "', '" + formater.format(pn.getNgayNhap()) + "', " + String.valueOf(pn.getTongTriGia()) + ", N'" + pn.getGhiChu() + "')";
        ResultSet rs = this.st.executeQuery(sql);
        rs.first();
        
        return rs.getString("SoPN");
    }
    
    public boolean update(PhieuNhap pn) throws SQLException {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
                
        String sql = "UPDATE PhieuNhap SET MaNV = '" + pn.getMaNV()+ "', MaNCC = '" + pn.getMaNCC()+ "', NgayNhap = '" + formater.format(pn.getNgayNhap()) + "', TongTriGia = " + String.valueOf(pn.getTongTriGia()) + ", GhiChu = N'" + pn.getGhiChu() + "' WHERE SoPN = '" +pn.getSoPN()+ "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String sopn) throws SQLException {
        new QuanLyChiTietPhieuNhap().deleteBySoPN(sopn);
        String sql = "EXEC sp_XoaPhieuNhap '" + sopn + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
}
