/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class HoaDon {
    private String SoHD;
    private String MaNV;
    private String SoBan;
    private Date NgayBan;
    private String TrangThai;
    private String GhiChu;

    public HoaDon(String SoHD, String MaNV, String SoBan, Date NgayBan, String TrangThai, String GhiChu) {
        this.SoHD = SoHD;
        this.MaNV = MaNV;
        this.SoBan = SoBan;
        this.NgayBan = NgayBan;
        this.TrangThai = TrangThai;
        this.GhiChu = GhiChu;
    }

    public String getSoHD() {
        return SoHD;
    }

    public void setSoHD(String SoHD) {
        this.SoHD = SoHD;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public String getSoBan() {
        return SoBan;
    }

    public void setSoBan(String SoBan) {
        this.SoBan = SoBan;
    }

    public Date getNgayBan() {
        return NgayBan;
    }

    public void setNgayBan(Date NgayBan) {
        this.NgayBan = NgayBan;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String GhiChu) {
        this.GhiChu = GhiChu;
    }
    
    public ChiTietHoaDon[] getDanhSachMonAn() throws SQLException {
        return new QuanLyChiTietHoaDon().getBySoHD(this.SoHD);
    }
    
    public NhanVien getNhanVien() throws SQLException {
        return new QuanLyNhanVien().find(this.MaNV);
    }
    
    public long getTongTien() throws SQLException {
        long tongtien;
        ChiTietHoaDon[] dsma = new QuanLyChiTietHoaDon().getBySoHD(this.SoHD);
        
        tongtien = 0;
        for(ChiTietHoaDon ma : dsma){
            tongtien += ma.getDonGiaBan() * ma.getSoLuong();
        }
        
        return tongtien;
    }
}
