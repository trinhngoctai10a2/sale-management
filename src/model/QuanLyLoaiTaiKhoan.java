/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Admin
 */
public class QuanLyLoaiTaiKhoan {
    private final Statement st;

    public QuanLyLoaiTaiKhoan() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public LoaiTaiKhoan find(String maltk) throws SQLException {
        String sql = "SELECT * FROM LoaiTaiKhoan WHERE MaLoaiTK = '" + maltk + "'";
        ResultSet rs = this.st.executeQuery(sql);
        
        String tenltk, ghichu;

        rs.first();
        tenltk = rs.getString("TenLoaiTK");
        ghichu = rs.getString("GhiChu");

        return new LoaiTaiKhoan(maltk, tenltk, ghichu);
    }
}
