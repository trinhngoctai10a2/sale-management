/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class QuanLyNhaCungCap {
    private final Statement st;

    public QuanLyNhaCungCap() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<NhaCungCap> getAll(String search) throws SQLException {
        ArrayList<NhaCungCap> ncc;
        String sql = "SELECT * FROM NhaCungCap";
        
        if(!search.equals("")){
            sql += " WHERE MaNCC LIKE '%" + search + "%' OR TenNCC LIKE N'%" + search + "%'";
        }
        ResultSet rs = this.st.executeQuery(sql);
        
        ncc = new ArrayList<>();
        
        while(rs.next()){
            String mancc, tenncc, diachi, dienthoai, email, website;
            
            mancc = rs.getString("MaNCC");
            tenncc = rs.getString("TenNCC");
            diachi = rs.getString("DiaChi");
            dienthoai = rs.getString("DienThoai");
            email = rs.getString("Email");
            website = rs.getString("Website");
            
            ncc.add(new NhaCungCap(mancc, tenncc, diachi, dienthoai, email, website));
        }
        
        return ncc;
    }
    
    public boolean add(NhaCungCap ncc) throws SQLException {
        String sql = "INSERT INTO NhaCungCap VALUES('" + ncc.getMaNCC()+ "', N'" + ncc.getTenNCC()+ "', N'" + ncc.getDiaChi()+ "', '" + ncc.getDienThoai() + "', '" + ncc.getEmail()+ "', '" + ncc.getWebsite() + "')";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean update(NhaCungCap ncc) throws SQLException {
        String sql = "UPDATE NhaCungCap SET TenNCC = N'" + ncc.getTenNCC()+ "', DiaChi = N'" + ncc.getDiaChi()+ "', DienThoai = '" + ncc.getDienThoai() + "', Email = '" + ncc.getEmail()+ "', Website = '" + ncc.getWebsite() + "' WHERE MaNCC = '" + ncc.getMaNCC() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String mancc) throws SQLException {
        String sql = "EXEC sp_XoaNhaCungCap '" + mancc + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
}
