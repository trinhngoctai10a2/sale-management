/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class QuanLyBan {
    private final Statement st;

    public QuanLyBan() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<Ban> getAll(String search) throws SQLException {
        ArrayList<Ban> ban;
        String sql = "SELECT * FROM Ban";
        
        if(!search.equals("")){
            sql += " WHERE SoBan LIKE '%" + search + "%' OR TenBan LIKE N'%" + search + "%'";
        }
        ResultSet rs = this.st.executeQuery(sql);
        
        ban = new ArrayList<>();
        while(rs.next()){
            String soban, tenban, trangthai, ghichu;
            
            soban = rs.getString("SoBan");
            tenban = rs.getString("TenBan");
            trangthai = rs.getString("TrangThai");
            ghichu = rs.getString("GhiChu");
            
            ban.add(new Ban(soban, tenban, trangthai, ghichu));
        }
        
        return ban;
    }
    
    public boolean add(Ban ban) throws SQLException {
        String sql = "INSERT INTO Ban VALUES('" + ban.getSoBan()+ "', N'" + ban.getTenBan()+ "', N'" + ban.getTrangThai() + "', N'" + ban.getGhiChu() + "')";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean update(Ban ban) throws SQLException {
        String sql = "UPDATE Ban SET TenBan = N'" + ban.getTenBan()+ "', TrangThai = N'" + ban.getTrangThai() + "', GhiChu = N'" + ban.getGhiChu() + "' WHERE SoBan = '" + ban.getSoBan() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String soban) throws SQLException {
        String sql = "EXEC sp_XoaBan '" + soban + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public Ban find(String soban) throws SQLException {
        String sql = "SELECT * FROM Ban WHERE SoBan = '" + soban + "'";
        ResultSet rs = this.st.executeQuery(sql);
        
        rs.first();
        return new Ban(soban, rs.getString("TenBan"), rs.getString("TrangThai"), rs.getString("GhiChu"));
    }
    
}
