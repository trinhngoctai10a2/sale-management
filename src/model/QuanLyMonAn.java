/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class QuanLyMonAn {
    private final Statement st;

    public QuanLyMonAn() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<MonAn> getAll(String search) throws SQLException {
        ArrayList<MonAn> ma;
        String sql = "SELECT * FROM MonAn";
        
        if(!search.equals("")){
            sql += " WHERE MaMA LIKE '%" + search + "%' OR TenMA LIKE N'%" + search + "%'";
        }
        ResultSet rs = this.st.executeQuery(sql);
        
        ma = new ArrayList<MonAn>();
        
        while(rs.next()){
            String mama, tenma, ghichu;
            long giaban;
            
            mama = rs.getString("MaMA");
            tenma = rs.getString("TenMA");
            giaban = rs.getLong("GiaBan");
            ghichu = rs.getString("GhiChu");
            
            ma.add(new MonAn(mama, tenma, giaban, ghichu));
        }
        
        return ma;
    }
    
    public boolean add(MonAn ma) throws SQLException {
        String sql = "INSERT INTO MonAn VALUES('" + ma.getMaMA() + "', N'" + ma.getTenMA() + "', " + String.valueOf(ma.getGiaBan()) + ", N'" + ma.getGhiChu() + "')";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean update(MonAn ma) throws SQLException {
        String sql = "UPDATE MonAn SET TenMA = N'" + ma.getTenMA() + "', GiaBan = " + String.valueOf(ma.getGiaBan()) + ", GhiChu = N'" + ma.getGhiChu() + "' WHERE MaMA = '" + ma.getMaMA() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String mama) throws SQLException {
        String sql = "EXEC sp_XoaMonAn '" + mama + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public MonAn find(String mama) throws SQLException {
        String sql = "SELECT * FROM MonAN WHERE MaMA = '" + mama + "'";
        ResultSet rs = this.st.executeQuery(sql);
        
        String tenma, ghichu;
        long giaban;

        rs.first();
        tenma = rs.getString("TenMA");
        giaban = rs.getLong("GiaBan");
        ghichu = rs.getString("GhiChu");

        return new MonAn(mama, tenma, giaban, ghichu);
    }
}
