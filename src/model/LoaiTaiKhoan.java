/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class LoaiTaiKhoan {
    private String MaLoaiTK;
    private String TenLoaiTK;
    private String GhiChu;

    public LoaiTaiKhoan(String MaLoaiTK, String TenLoaiTK, String GhiChu) {
        this.MaLoaiTK = MaLoaiTK;
        this.TenLoaiTK = TenLoaiTK;
        this.GhiChu = GhiChu;
    }

    public String getMaLoaiTK() {
        return MaLoaiTK;
    }

    public void setMaLoaiTK(String MaLoaiTK) {
        this.MaLoaiTK = MaLoaiTK;
    }

    public String getTenLoaiTK() {
        return TenLoaiTK;
    }

    public void setTenLoaiTK(String TenLoaiTK) {
        this.TenLoaiTK = TenLoaiTK;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String GhiChu) {
        this.GhiChu = GhiChu;
    }
}
