/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Admin
 */
public class QuanLyChiTietHoaDon {
    private final Statement st;

    public QuanLyChiTietHoaDon() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ChiTietHoaDon[] getBySoHD(String sohd) throws SQLException {
        ChiTietHoaDon cthd[];
        String sql = "SELECT * FROM ChiTietHoaDon WHERE SoHD = '" + sohd + "'";
        
        ResultSet rs = this.st.executeQuery(sql);
        
        rs.last();
        int n = rs.getRow();
        cthd = new ChiTietHoaDon[n];
        
        
        rs.beforeFirst();
        int i = 0;
        while(rs.next()){
            String mama;
            int soluong;
            long dongiaban;
            
            mama = rs.getString("MaMA");
            soluong = rs.getInt("SoLuong");
            dongiaban = rs.getLong("DonGiaBan");
            
            cthd[i] = new ChiTietHoaDon(sohd, mama, dongiaban, soluong);
            i++;
        }
        
        return cthd;
    }
    
    public boolean add(ChiTietHoaDon cthd) throws SQLException {
        String sql = "INSERT INTO ChiTietHoaDon VALUES('" + cthd.getSoHD()+ "', '" + cthd.getMaMA() + "', " + String.valueOf(cthd.getDonGiaBan()) + ", " + String.valueOf(cthd.getSoLuong()) + ")";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean update(ChiTietHoaDon cthd) throws SQLException {
        String sql = "UPDATE ChiTietHoaDon SET DonGiaBan = " + String.valueOf(cthd.getDonGiaBan())+ ", SoLuong = " + String.valueOf(cthd.getSoLuong())+ " WHERE SoHD = '" + cthd.getSoHD()+ "' AND MaMA = '" + cthd.getMaMA() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String sohd, String mama) throws SQLException {
        String sql = "DELETE FROM ChiTietHoaDon WHERE SoHD = '" + sohd + "' AND MaMA = '" + mama + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean deleteBySoHD(String sohd) throws SQLException {
        String sql = "DELETE FROM ChiTietHoaDon WHERE SoHD = '" + sohd + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
}
