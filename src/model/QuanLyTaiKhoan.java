/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author Admin
 */
public class QuanLyTaiKhoan {
    private final Statement st;

    public QuanLyTaiKhoan() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public TaiKhoan find(String tdn) throws SQLException {
        String sql = "SELECT * FROM TaiKhoan WHERE TenDangNhap = '" + tdn + "'";
        
        ResultSet rs = this.st.executeQuery(sql);
        String mk, maloaitk, manv;
        
        rs.first();
        mk = rs.getString("MatKhau");
        maloaitk = rs.getString("MaLoaiTK");
        manv = rs.getString("MaNV");
        
        return new TaiKhoan(tdn, mk, maloaitk, manv);
    }
    
    public boolean update(TaiKhoan tk) throws SQLException {
        String sql = "UPDATE TaiKhoan SET MatKhau = '" + tk.getMatKhau()+ "', MaLoaiTK = '" + tk.getMaLTK() + "', MaNV = '" + tk.getMaNV() + "' WHERE TenDangNhap = '" + tk.getTenDangNhap() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean DangNhap(String tdn, String mk) throws SQLException {
        TaiKhoan tk;
        tk = find(tdn);
        return BCrypt.checkpw(mk, tk.getMatKhau());
    }
    
    public String hash(String mk){
        return BCrypt.hashpw(mk, BCrypt.gensalt(12));
    }
}
