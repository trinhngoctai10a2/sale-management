/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class PhieuNhap {
    private String SoPN;
    private String MaNV;
    private String MaNCC;
    private Date NgayNhap;
    private double TongTriGia;
    private String GhiChu;

    public PhieuNhap(String SoPN, String MaNV, String MaNCC, Date NgayNhap, double TongTriGia, String GhiChu){
        this.SoPN = SoPN;
        this.MaNV = MaNV;
        this.MaNCC = MaNCC;
        this.NgayNhap = NgayNhap;
        this.TongTriGia = TongTriGia;
        this.GhiChu = GhiChu;
    }

   

    public String getSoPN() {
        return SoPN;
    }

    public void setSoPN(String SoPN) {
        this.SoPN = SoPN;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    public String getMaNCC() {
        return MaNCC;
    }

    public void setMaNCC(String MaNCC) {
        this.MaNCC = MaNCC;
    }

    public Date getNgayNhap() {
        return NgayNhap;
    }

    public void setNgayNhap(Date NgayNhap) {
        this.NgayNhap = NgayNhap;
    }

    public double getTongTriGia() {
        return TongTriGia;
    }

    public void setTongTriGia(double TongTriGia) {
        this.TongTriGia = TongTriGia;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String GhiChu) {
        this.GhiChu = GhiChu;
    }
}
