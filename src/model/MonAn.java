/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Admin
 */
public class MonAn {
    private String MaMA;
    private String TenMA;
    private long GiaBan;
    private String GhiChu;

    public MonAn(String MaMA, String TenMA, long GiaBan, String GhiChu) {
        this.MaMA = MaMA;
        this.TenMA = TenMA;
        this.GiaBan = GiaBan;
        this.GhiChu = GhiChu;
    }

    public String getMaMA() {
        return MaMA;
    }

    public void setMaMA(String MaMA) {
        this.MaMA = MaMA;
    }

    public String getTenMA() {
        return TenMA;
    }

    public void setTenMA(String TenMA) {
        this.TenMA = TenMA;
    }

    public long getGiaBan() {
        return GiaBan;
    }

    public void setGiaBan(long GiaBan) {
        this.GiaBan = GiaBan;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String GhiChu) {
        this.GhiChu = GhiChu;
    }
}
