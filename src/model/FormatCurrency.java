/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.DecimalFormat;

/**
 *
 * @author Admin
 */
public class FormatCurrency {
    public static String format(long money){
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(money);
    }
    
}
