/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class QuanLyNguyenLieu {
    private final Statement st;

    public QuanLyNguyenLieu() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<NguyenLieu> getAll(String search) throws SQLException {
        ArrayList<NguyenLieu> nl;
        String sql = "SELECT * FROM NguyenLieu";
        
        if(!search.equals("")){
            sql += " WHERE MaNL LIKE '%" + search + "%' OR TenNL LIKE N'%" + search + "%'";
        }
        ResultSet rs = this.st.executeQuery(sql);
        
        nl = new ArrayList<>();
        
        while(rs.next()){
            String manl, tennl, donvitinh, ghichu;
            int tonkho;
            
            manl = rs.getString("MaNL");
            tennl = rs.getString("TenNL");
            donvitinh = rs.getString("DonViTinh");
            tonkho = rs.getInt("TonKho");
            ghichu = rs.getString("GhiChu");
            
            nl.add(new NguyenLieu(manl, tennl, donvitinh, tonkho, ghichu));
        }
        
        return nl;
    }
    
    public boolean add(NguyenLieu nl) throws SQLException {
        String sql = "INSERT INTO NguyenLieu VALUES('" + nl.getMaNL()+ "', N'" + nl.getTenNL()+ "', N'" + nl.getDonViTinh() + "', " + String.valueOf(nl.getTonKho()) + ", N'" + nl.getGhiChu() + "')";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean update(NguyenLieu nl) throws SQLException {
        String sql = "UPDATE NguyenLieu SET TenNL = N'" + nl.getTenNL()+ "', DonViTinh = N'" + nl.getDonViTinh() + "', TonKho = " + String.valueOf(nl.getTonKho()) + ", GhiChu = N'" + nl.getGhiChu() + "' WHERE MaNL = '" + nl.getMaNL() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String manl) throws SQLException {
        String sql = "EXEC sp_XoaNguyenLieu '" + manl + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
}
