/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class QuanLyChiTietPhieuNhap {
    private final Statement st;

    public QuanLyChiTietPhieuNhap() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<ChiTietPhieuNhap> getBySoPN(String sopn) throws SQLException {
        ArrayList<ChiTietPhieuNhap> ctpn;
        String sql = "SELECT * FROM ChiTietPhieuNhap WHERE SoPN = " + sopn;
        
        ResultSet rs = this.st.executeQuery(sql);
        
        ctpn = new ArrayList<ChiTietPhieuNhap>();
        
        while(rs.next()){
            String manl;
            int soluong;
            double dongianhap;
            
            manl = rs.getString("MaNL");
            soluong = rs.getInt("SoLuong");
            dongianhap = rs.getDouble("DonGiaNhap");
            
            ctpn.add(new ChiTietPhieuNhap(sopn, manl, dongianhap, soluong));
        }
        
        return ctpn;
    }
    
    public boolean add(ChiTietPhieuNhap ctpn) throws SQLException {
        String sql = "INSERT INTO ChiTietPhieuNhap VALUES('" + ctpn.getSoPN()+ "', '" + ctpn.getMaNL() + "', " + String.valueOf(ctpn.getDonGiaNhap()) + ", " + String.valueOf(ctpn.getSoLuong()) + ")";
        int r = this.st.executeUpdate(sql);
        if(r > 0)
            return true;
        return false;
    }
    
    public boolean update(ChiTietPhieuNhap ctpn) throws SQLException {
        String sql = "UPDATE ChiTietPhieuNhap SET DonGiaNhap = " + String.valueOf(ctpn.getDonGiaNhap())+ ", SoLuong = " + String.valueOf(ctpn.getSoLuong())+ " WHERE SoPN = '" + ctpn.getSoPN()+ "' AND MaNL = '" + ctpn.getMaNL() + "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String sopn, String manl) throws SQLException {
        String sql = "DELETE FROM ChiTietPhieuNhap WHERE SoPN = '" + sopn + "' AND MaNL = '" + manl + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean deleteBySoPN(String sopn) throws SQLException {
        String sql = "DELETE FROM ChiTietPhieuNhap WHERE SoPN = '" + sopn + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
}
