/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class QuanLyHoaDon {
    private final Statement st;

    public QuanLyHoaDon() throws SQLException {
        this.st = KetNoiCSDL.getStatement();
    }
    
    public ArrayList<HoaDon> getAll(String search) throws SQLException {
        ArrayList<HoaDon> hd;
        String sql = "SELECT * FROM HoaDon";
        
        if(!search.equals("")){
            sql += " WHERE SoHD LIKE '%" + search + "%' OR MaNV LIKE N'%" + search + "%' OR SoBan LIKE N'%" + search + "%'";
        }
        
        ResultSet rs = this.st.executeQuery(sql);
        
        hd = new ArrayList<>();
        while(rs.next()){
            String sohd, manv, soban, trangthai,ghichu;
            Date ngayban;
            
            sohd = rs.getString("SoHD");
            manv = rs.getString("MaNV");
            soban = rs.getString("SoBan");
            ngayban = rs.getDate("NgayBan");
            trangthai = rs.getString("TrangThai");
            ghichu = rs.getString("GhiChu");
            
            hd.add(new HoaDon(sohd, manv, soban, ngayban, trangthai, ghichu));
        }
        
        return hd;
    }
    
    public HoaDon[] getByDate(String tungay, String denngay) throws SQLException {
        HoaDon hd[];
        String sql = "SELECT * FROM HoaDon WHERE NgayBan BETWEEN '" + tungay + "' AND '" + denngay + "'";
        
        ResultSet rs = this.st.executeQuery(sql);
        
        rs.last();
        int n = rs.getRow();
        hd = new HoaDon[n];
        
        rs.beforeFirst();
        int i = 0;
        while(rs.next()){
            String sohd, manv, soban, trangthai,ghichu;
            Date ngayban;
            
            sohd = rs.getString("SoHD");
            manv = rs.getString("MaNV");
            soban = rs.getString("SoBan");
            ngayban = rs.getDate("NgayBan");
            trangthai = rs.getString("TrangThai");
            ghichu = rs.getString("GhiChu");
            
            hd[i] = new HoaDon(sohd, manv, soban, ngayban, trangthai, ghichu);
            i++;
        }
        
        return hd;
    }
    
    public String add(HoaDon hd) throws SQLException {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
        
        String sql = "INSERT INTO HoaDon (MaNV, SoBan, NgayBan, TrangThai, GhiChu) OUTPUT INSERTED.SoHD VALUES('" + hd.getMaNV()+ "', '" + hd.getSoBan()+ "', '" + formater.format(hd.getNgayBan()) + "', N'" + hd.getTrangThai()+ "', N'" + hd.getGhiChu() + "')";
       
        ResultSet rs = this.st.executeQuery(sql);
        rs.first();
        
        return rs.getString("SoHD");
    }
    
    public boolean update(HoaDon hd) throws SQLException {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");
                
        String sql = "UPDATE HoaDon SET MaNV = '" + hd.getMaNV()+ "', SoBan = '" + hd.getSoBan()+ "', NgayBan = '" + formater.format(hd.getNgayBan()) + "', TrangThai = N'" + hd.getTrangThai() + "', GhiChu = N'" + hd.getGhiChu() + "' WHERE SoHD = '" + hd.getSoHD()+ "'";
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public boolean delete(String sohd) throws SQLException {
        new QuanLyChiTietHoaDon().deleteBySoHD(sohd);
        String sql = "EXEC sp_XoaHoaDon '" + sohd + "'";
        
        int r = this.st.executeUpdate(sql);
        return r > 0;
    }
    
    public HoaDon find(String sohd) throws SQLException {
        String sql = "SELECT * FROM HoaDon WHERE SoHD = '" + sohd + "'" ;
        
        ResultSet rs = this.st.executeQuery(sql);
        rs.first();
        
        String manv, soban, trangthai,ghichu;
        Date ngayban;
        
        manv = rs.getString("MaNV");
        soban = rs.getString("SoBan");
        ngayban = rs.getDate("NgayBan");
        trangthai = rs.getString("TrangThai");
        ghichu = rs.getString("GhiChu");
        
        return new HoaDon(sohd, manv, soban, ngayban, trangthai, ghichu);
    }
    
    public HoaDon getHoaDonTheoBan(String soban) throws SQLException {
        String sql = "SELECT * FROM HoaDon WHERE SoBan = '" + soban + "' AND TrangThai = N'Chưa thanh toán'";
        ResultSet rs = this.st.executeQuery(sql);
        rs.first();
        
        String sohd, manv, trangthai, ghichu;
        Date ngayban;
        
        sohd = rs.getString("SoHD");
        manv = rs.getString("MaNV");
        ngayban = rs.getDate("NgayBan");
        trangthai = rs.getString("TrangThai");
        ghichu = rs.getString("GhiChu");
        
        return new HoaDon(sohd, manv, soban, ngayban, trangthai, ghichu);
    }
    
}
