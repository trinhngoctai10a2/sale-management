/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.SQLException;

/**
 *
 * @author Admin
 */
public class TaiKhoan {
    private String TenDangNhap;
    private String MatKhau;
    private String MaLTK;
    private String MaNV;

    public TaiKhoan(String TenDangNhap, String MatKhau, String MaLTK, String MaNV) {
        this.TenDangNhap = TenDangNhap;
        this.MatKhau = MatKhau;
        this.MaLTK = MaLTK;
        this.MaNV = MaNV;
    }

    public String getTenDangNhap() {
        return TenDangNhap;
    }

    public void setTenDangNhap(String TenDangNhap) {
        this.TenDangNhap = TenDangNhap;
    }

    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String MatKhau) {
        this.MatKhau = MatKhau;
    }

    public String getMaLTK() {
        return MaLTK;
    }

    public void setMaLTK(String MaLTK) {
        this.MaLTK = MaLTK;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }
    
    public NhanVien getNhanVien() throws SQLException {
        return new QuanLyNhanVien().find(this.MaNV);
    }
    
    public LoaiTaiKhoan getLoaiTaiKhoan() throws SQLException {
        return new QuanLyLoaiTaiKhoan().find(this.MaLTK);
    }
}
